
module.exports = {
  key: 'uuid', // Default key for UUID in frontmatter
  uuidVersion: 'v4' // options are v1, v3, v4 and v5
}
