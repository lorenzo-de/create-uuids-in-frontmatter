import fs from 'fs'
import _ from 'lodash'
import replace from 'replace'
import uuid1 from 'uuid/v1'
import uuid3 from 'uuid/v3'
import uuid4 from 'uuid/v4'
import uuid5 from 'uuid/v5'
import walk from 'walk'

import configuration from '../configuration'

export const createUuid = (version = configuration.uuidVersion) => {
  const versions = ['v1', 'v3', 'v4', 'v5']
  if (!versions.includes(version)) throw (new Error('Argument must be v1, v3, v4 or v5.'))
  if (version === 'v1') return uuid1()
  if (version === 'v3') return uuid3()
  if (version === 'v4') return uuid4()
  if (version === 'v5') return uuid5()
}

export const replaceUuid = (
  pathToDirectory,
  key = configuration.key,
  replacerHandler = replace,
  createUuidHandler = createUuid
) => {
  if (!_.isString(pathToDirectory)) {
    throw (
      new Error('pathToDirectory argument is missing or is not of type string.')
    )
  } else if (!_.isString(key)) {
    throw (
      new Error('key argument is missing or is not of type string.')
    )
  } else if (!_.isFunction(replacerHandler)) {
    throw (
      new Error('replaceHandler must be a function.')
    )
  } else if (!_.isFunction(createUuidHandler)) {
    throw (
      new Error('createUuidHandler must be a function.')
    )
  } else {
    fs.lstatSync(pathToDirectory).isDirectory()

    const walker = walk.walk(pathToDirectory)
    const regex = `${key}: (.*)`

    walker.on('file', (root, stats, next) => {
      const pathToFile = `${root}/${stats.name}`
      console.log(`Processing file ${pathToFile}`)

      let uuid = createUuidHandler()
      let replacement = `${key}: ${uuid}`

      replacerHandler({
        regex: regex,
        replacement: replacement,
        paths: [pathToFile],
        recursive: false,
        silent: true,
        include: '*.md'
      })
      next()
    })
  }
}
