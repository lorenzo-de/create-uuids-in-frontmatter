# create-uuids-in-frontmatter

Walk a directory with markdown files and replace all UUIDs in frontmatter with new randomly created version 4 UUID.

`npm run start --directory=/path/to/directory --key=uuid`

where `directory` is the directory to scan recursively for Markdown files and `uuid` is the key where the UUID is stored in the files frontmatter.

Existing UUIDs or other values stored in that key are replaced.

