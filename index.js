import { replaceUuid } from './src'

console.log(`
  Trying to replace all UUIDs in frontmatter.\n
  Script arguments are:\n
  - Uuid key in frontmatter: ${process.env.npm_config_key}\n
  - Walked directory: ${process.env.npm_config_directory}\n`
)

replaceUuid(
  process.env.npm_config_directory,
  process.env.npm_config_key
)
