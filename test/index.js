/* eslint-env node, mocha */

import assert from 'assert'
import _ from 'lodash'
import validator from 'validator'

import { createUuid, replaceUuid } from '../src/'

describe('#createUuid()', () => {
  it('throws error if argument is not v1, v3, v4 or v5', (done) => {
    assert.throws(() => createUuid('v6'), Error)
    done()
  })
  it('returns a v4 uuid if argument is v4', (done) => {
    assert.ok(validator.isUUID(createUuid('v4'), 4))
    done()
  })
  it('returns returns a string', (done) => {
    assert.ok(_.isString(createUuid('v1')))
    done()
  })
})

describe('#replaceUuid()', () => {
  it('throws error if pathToDirectory argument is not of type string', (done) => {
    assert.throws(() => replaceUuid({}, 'uuid'), Error)
    done()
  })
  it('throws error if pathToDirectory is not a pathToDirectory', (done) => {
    assert.throws(() => replaceUuid('/path/to/nowhere/', 'uuid'), Error)
    done()
  })
  it('throws error if key argument is not of type string', (done) => {
    assert.throws(() => replaceUuid(process.cwd(), {}), Error)
    done()
  })
  it('throws error if replacerHandler is not a function', (done) => {
    assert.throws(() => replaceUuid(process.cwd(), 'uuid', ''), Error)
    done()
  })
  it('throws error if createUuidHandler is not a function', (done) => {
    assert.throws(() => replaceUuid(process.cwd(), 'uuid', () => {}, ''), Error)
    done()
  })
})
